package DAO.Beans;

import DAO.Entities.Patient;
import exceptions.DAOException;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kate on 13.03.2017.
 */
@Stateless//(name = "HospitalEJB")
//@Remote({PatientSession.class})
//@Local({LocalPatientSession.class})
public class PatientSessionBean implements PatientSession, LocalPatientSession{
    //private static Logger logger = LogManager.getLogger(PatientSessionBean.class);
    /**
     * Session factory that uses for getting sessions.
     */
    private EntityManagerFactory factory;
    /**
     * Current session.
     */
    @PersistenceContext(unitName = "HospitalPersistenceUnit")
    protected EntityManager entityManager;

    @PostConstruct
    public void init() {
        try {
            factory = Persistence.createEntityManagerFactory( "HospitalPersistenceUnit" );
            entityManager = factory.createEntityManager();
        } catch (Throwable ex) {
            //logger.fatal("Can't create entity manager factory.", ex);
            ex.printStackTrace();
        }
    }
    /**
     * Returns all patients.
     * @return List of patients.
     * @throws DAOException Throws when some errors happens.
     */
    @Override
    public List<Patient> getAll() throws  DAOException{
        List<Patient> patientsList = new ArrayList<>();
        try{
            List patients = entityManager.createNamedQuery("getAllPatientsQuery").getResultList();
            for (Iterator iterator =
                 patients.iterator(); iterator.hasNext();){
                Patient patient = (Patient) iterator.next();
                patientsList.add(patient);
            }
        }catch (Exception e) {
            //e.printStackTrace();
            throw new DAOException("Error while getting list of patients", e);
        }
        return patientsList;
    }
    /**
     * Returns patients by doctor ID.
     * @param doctorId ID of doctor.
     * @return List of patients.
     * @throws DAOException Throws when some errors happens.
     */
    @Override
    public List<Patient> getPatientsByDoctorId(int doctorId)  throws  DAOException{
        List<Patient> patientsList = new ArrayList<>();
        try{
            List patients = entityManager.createNamedQuery("getPatientsByDoctorIdQuery")
                    .setParameter("doctor_id", doctorId).getResultList();
            for (Iterator iterator =
                 patients.iterator(); iterator.hasNext();){
                Patient patient = (Patient) iterator.next();
                patientsList.add(patient);
            }
        }catch (Exception e) {
            //e.printStackTrace();
            throw new DAOException("Error while getting list of patients", e);
        }
        return patientsList;
    }
    @PreDestroy
    public void close() {
        if(entityManager != null)
            entityManager.close();
        if (factory!=null) {
            factory.close();
        }
    }

}
