package DAO.Beans;

import DAO.Entities.Patient;
import exceptions.DAOException;
import javax.ejb.Remote;
import java.util.List;

/**
 * Created by Kate on 13.03.2017.
 */
@Remote
public interface PatientSession {
    /**
     * Returns all patients.
     * @return List of patients.
     * @throws DAOException Throws when some errors happens.
     */
    public List<Patient> getAll()  throws  DAOException;
    /**
     * Returns patients by doctor ID.
     * @param doctorId ID of doctor.
     * @return List of patients.
     * @throws DAOException Throws when some errors happens.
     */
    public List<Patient> getPatientsByDoctorId(int doctorId)  throws  DAOException;
}
