package DAO.Beans;

import DAO.Entities.Patient;
import exceptions.DAOException;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by Kate on 18.03.2017.
 */
@Local
public interface LocalPatientSession {
    /**
     * Returns all patients.
     * @return List of patients.
     * @throws DAOException Throws when some errors happens.
     */
    public List<Patient> getAll()  throws  DAOException;
    /**
     * Returns patients by doctor ID.
     * @param doctorId ID of doctor.
     * @return List of patients.
     * @throws DAOException Throws when some errors happens.
     */
    public List<Patient> getPatientsByDoctorId(int doctorId)  throws  DAOException;
}
