package DAO.Enums;

/**
 * Enum PostTypes.
 */
public enum PostTypes {
    /**
     * Posts: INTERN, DOCTOR, CHIEF_PHYSICIAN.
     */
    INTERN, DOCTOR, CHIEF_PHYSICIAN;
    /**
     * Converts enum value to String.
     * @return Type as String.
     */
    @Override
    public String toString() {
        switch(this) {
            case INTERN: return "intern";
            case DOCTOR: return "doctor";
            case CHIEF_PHYSICIAN: return "chief physician";
            default: throw new IllegalArgumentException();
        }
    }
}
