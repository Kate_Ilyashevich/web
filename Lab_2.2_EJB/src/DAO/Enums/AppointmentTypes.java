package DAO.Enums;

/**
 * Enum AppointmentTypes.
 */
public enum AppointmentTypes {
    /**
     * Types of appointment: PROCEDURE, DRUGS, OPERATION.
     */
    PROCEDURE, DRUGS, OPERATION;

    /**
     * Converts enum value to String.
     * @return Type as String.
     */
    @Override
    public String toString() {
        switch(this) {
            case PROCEDURE: return "procedure";
            case DRUGS: return "drugs";
            case OPERATION: return "operation";
            default: throw new IllegalArgumentException();
        }
    }
}
