package DAO.Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Queries to entity Patient.
 */
@NamedQueries({
        /**
         * Query thar returns all patients.
         */
        @NamedQuery(
                name = "getAllPatientsQuery",
                query = "from Patient"
        ),
        /**
         * Query that returns patients by doctor ID.
         */
        @NamedQuery(
                name = "getPatientsByDoctorIdQuery",
                query = "select distinct p from Patient as p, " +
                        "History as h " +
                        "where p.id = h.patientId " +
                        "and h.doctorId = :doctor_id"
        )
}
)
/**
 * Class Patient.
 */
@Entity
@Table(name = "patient", schema = "hospital")
public class Patient implements Serializable{
    /**
     * Names of columns.
     */
    public static final String ID = "id",
            NAME = "name",
            SURNAME = "surname";
    /**
     * ID of patient.
     */
    private int id;
    /**
     * Name of patient.
     */
    private String name;
    /**
     * Surname of patient.
     */
    private String surname;

    /**
     * All histories of this patient.
     */
    @OneToMany()    @JoinColumn(name = "patient_id",referencedColumnName = "id")
    private List<History> histories;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Patient patient = (Patient) o;

        if (id != patient.id) return false;
        if (name != null ? !name.equals(patient.name) : patient.name != null) return false;
        if (surname != null ? !surname.equals(patient.surname) : patient.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        return result;
    }

    /**
     * Returns patient as String.
     * @return Patient as String.
     */
    @Override
    public String toString()
    {
        return String.format("%s: %-5d %s: %-10s %s: %-10s",
                ID, id,
                NAME, name,
                SURNAME, surname);
    }
}
