package DAO.Entities;

import DAO.Enums.AppointmentTypes;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Queries to entity Appointment.
 */
@NamedQueries({
        /**
         * Query that returns all appointments of particular patient.
         */
        @NamedQuery(
                name = "getAllAppointmentsByPatientIdQuery",
                query = "select a from History as h, " +
                        "Appointment as a " +
                        "where a.historyId = h.id and h.patientId = :patient_id"
        )
}
)
/**
 * Class Appointment.
 */
@Entity
@Table(name = "appointments", schema = "hospital")
public class Appointment implements Serializable{
    /**
     * String NULL.
     */
    public static final String NULL = "NULL";
    /**
     * Names of columns.
     */
    public static final String ID = "id",
            HISTORY_ID = "history_id",
            DATE = "date",
            APPOINTMENT = "appointment",
            DESCRIPTION = "description";
    /**
     * ID of appointment.
     */
    private int id;
    /**
     * ID of history this appointment belongs to.
     */
    private int historyId;
    /**
     * Date of making appointment.
     */
    private Date date;
    /**
     * Type of appointment.
     */
    private int appointment;
    /**
     * Description of appointment.
     */
    private String description;
    /**
     * Constructor.
     */
    public Appointment()
    {
    }
    /**
     * Constructor.
     * @param id ID of appointment.
     * @param historyId ID of history this appointment belongs to.
     * @param date Date of making appointment.
     * @param appointment Type of appointment.
     * @param description Description of appointment.
     */
    public Appointment(int id, int historyId, Date date, int appointment, String description)
    {
        this.id = id;
        this.historyId = historyId;
        this.date = date;
        this.appointment = appointment;
        this.description = description;
    }

    /**
     * History associated with this appointment.
     */
    /*@ManyToOne(targetEntity = History.class)
    private History history;*/

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "history_id")
    public int getHistoryId() {
        return historyId;
    }
    public void setHistoryId(int historyId) {
        this.historyId = historyId;
    }

    @Basic
    @Column(name = "date")
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "appointment")
    public int getAppointment() {
        return appointment;
    }
    public void setAppointment(int appointment) {
        this.appointment = appointment;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Appointment that = (Appointment) o;

        if (id != that.id) return false;
        if (appointment != that.appointment) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + appointment;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return String.format("%s: %-5d %s: %-5d %s: %-15s %s: %-15s %s: %-10s",
                ID, id,
                HISTORY_ID, historyId,
                DATE, date != null ? date.toString() : NULL,
                APPOINTMENT, AppointmentTypes.values()[appointment].toString(),
                DESCRIPTION, description);
    }
}
