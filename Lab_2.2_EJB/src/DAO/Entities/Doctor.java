package DAO.Entities;

import DAO.Enums.PostTypes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * Queries to entity Doctor.
 */
@NamedQueries({
        /**
         * Query thar returns all doctors.
         */
        @NamedQuery(
                name = "getAllDoctorsQuery",
                query = "FROM Doctor"
        )
}
)
/**
 * Class Doctor.
 */
@Entity
@Table(name = "doctor", schema = "hospital")
public class Doctor implements Serializable{
    /**
     * Names of columns.
     */
    public static final String ID = "id",
            NAME = "name",
            SURNAME = "surname",
            POST = "post";
    /**
     * ID of doctor.
     */
    private int id;
    /**
     * Name of doctor.
     */
    private String name;
    /**
     * Surname of doctor.
     */
    private String surname;
    /**
     * Post of doctor.
     */
    private int post;

    /**
     * Histories managed by this doctor.
     */
    @OneToMany()    @JoinColumn(name = "doctor_id",referencedColumnName = "id")
    private List<History> histories;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "post")
    public int getPost() {
        return post;
    }
    public void setPost(int post) {
        this.post = post;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Doctor doctor = (Doctor) o;

        if (id != doctor.id) return false;
        if (post != doctor.post) return false;
        if (name != null ? !name.equals(doctor.name) : doctor.name != null) return false;
        if (surname != null ? !surname.equals(doctor.surname) : doctor.surname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + post;
        return result;
    }

    /**
     * Returns doctor as String.
     * @return Doctor as String.
     */
    @Override
    public String toString()
    {
        return String.format("%s: %-5d %s: %-10s %s: %-10s %s: %-10s",
                ID, id,
                NAME, name,
                SURNAME, surname,
                POST, PostTypes.values()[post].toString());
    }
}
