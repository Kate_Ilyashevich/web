package DAO.Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.*;

/**
 * Queries to entity History.
 */
@NamedQueries({
        /**
         * Query thar returns all histories of particular patient.
         */
        @NamedQuery(
                name = "getAllHistoriesByPatientIdQuery",
                query = "from History as h where h.patientId = :patient_id"
        )
}
)
/**
 * Class History.
 */
@Entity
@Table(name = "histories", schema = "hospital")
public class History implements Serializable{
    /**
     * String NULL.
     */
    public static final String NULL = "NULL";
    /**
     * Names of columns.
     */
    public static final String ID = "id",
            PATIENT_ID = "patient_id",
            DOCTOR_ID = "doctor_id",
            START_DATE = "start_date",
            END_DATE = "end_date",
            DIAGNOSIS = "diagnosis";
    /**
     * ID of history.
     */
    private int id;
    /**
     * ID of patient.
     */
    private int patientId;
    /**
     * ID of doctor.
     */
    private int doctorId;
    /**
     * Start date of treatment.
     */
    private Date startDate;
    /**
     * End date of treatment.
     */
    private Date endDate;
    /**
     * Diagnosis.
     */
    private String diagnosis;
    /**
     * Doctor that treat patient with this history.
     */
    @ManyToOne(targetEntity = Doctor.class)
    private Doctor doctor;
    /**
     * Patient who own this history.
     */
    @ManyToOne(targetEntity = Patient.class)
    private Patient patient;
    /**
     * Set of appointments of this history.
     */
    @OneToMany()    @JoinColumn(name = "history_id",referencedColumnName = "id")
    private List<Appointment> appointments;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "patient_id")
    public int getPatientId() {
        return patientId;
    }
    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Basic
    @Column(name = "doctor_id")
    public int getDoctorId() {
        return doctorId;
    }
    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    @Basic
    @Column(name = "start_date")
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "end_date")
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "diagnosis")
    public String getDiagnosis() {
        return diagnosis;
    }
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        History history = (History) o;

        if (id != history.id) return false;
        if (startDate != null ? !startDate.equals(history.startDate) : history.startDate != null) return false;
        if (endDate != null ? !endDate.equals(history.endDate) : history.endDate != null) return false;
        if (diagnosis != null ? !diagnosis.equals(history.diagnosis) : history.diagnosis != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (diagnosis != null ? diagnosis.hashCode() : 0);
        return result;
    }

    /**
     * Returns history as String.
     * @return History as String.
     */
    @Override
    public String toString()
    {
        return String.format("%s: %-5d %s: %-5d %s: %-5d %s: %-14s %s: %-14s %s: %-15s",
                ID, id,
                PATIENT_ID, patientId,
                DOCTOR_ID, doctorId,
                START_DATE, startDate.toString(),
                END_DATE, endDate != null ? endDate.toString() : NULL,
                DIAGNOSIS, diagnosis != null ? diagnosis : NULL);
    }
}
